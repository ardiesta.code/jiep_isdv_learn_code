using System;
using System.Collections.Generic;

namespace LearCore.Models
{
    public partial class DeveloperForum
    {

        public int ForumId { get; set; }
        public string ForumName { get; set; }
    }
}
